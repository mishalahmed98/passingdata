//
//  BookViewController.swift
//  PassingData
//
//  Created by Денис Матвеев on 29/03/2020.
//  Copyright © 2020 Денис Матвеев. All rights reserved.
//

import UIKit

class BookViewController: UIViewController, UITextViewDelegate, BookViewControllerDelegate {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAuthor: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var ratingButton_1: UIButton!
    @IBOutlet weak var ratingButton_2: UIButton!
    @IBOutlet weak var ratingButton_3: UIButton!
    @IBOutlet weak var ratingButton_4: UIButton!
    @IBOutlet weak var ratingButton_5: UIButton!
    
    var book: Book? {
        didSet {
            if let updatedBook = book {
                updateBookClosure?(updatedBook)
            }
        }
    }

    var updateBookClosure: ((Book) -> Void)?
    
    @IBAction func ratingButtonTouch(_ sender: UIButton) {
        if let _ = book {
            book?.rating = sender.tag
            updateRatingView()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if reviewTextView.textColor == UIColor.secondaryLabel {
            reviewTextView.text = ""
            reviewTextView.textColor = UIColor.darkText
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if reviewTextView.text == "" {
            reviewTextView.text = "Add your review on this book here…"
            reviewTextView.textColor = UIColor.secondaryLabel
        }
        book?.review = reviewTextView.text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        reviewTextView.delegate = self
        updateControls()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(BookViewController.handleKeyboardDidShow(notification:)),
            name: UIResponder.keyboardDidShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(BookViewController.handleKeyboardDidHide(notification:)),
            name: UIResponder.keyboardDidHideNotification,
            object: nil
        )
    }
    
    @objc func handleKeyboardDidShow(notification: NSNotification) {
        guard let keyboardRect = notification
            .userInfo![UIResponder.keyboardFrameEndUserInfoKey]
            as? NSValue else { return }

        let frameKeyboard = keyboardRect.cgRectValue

        reviewTextView.contentInset = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: frameKeyboard.size.height,
            right: 0.0
        )

        view.layoutIfNeeded()
    }
    
    @objc func handleKeyboardDidHide(notification: NSNotification) {
        reviewTextView.contentInset = .zero
    }
    
    func updateControls() {
        self.title = book?.title
        labelTitle.text = book?.title
        labelAuthor.text = book?.author
        
        if let _ = book, let review = book?.review {
            reviewTextView.text = review
        } else {
            reviewTextView.text = "Add your review on this book here…"
            reviewTextView.textColor = UIColor.secondaryLabel
        }
        
        updateRatingView()
    }
    
    func updateRatingView() {
        if let _ = book, let rating = book?.rating {
            ratingButton_1.setImage(UIImage(systemName: rating >= 1 ? "star.fill" : "star"), for: .normal)
            ratingButton_2.setImage(UIImage(systemName: rating >= 2 ? "star.fill" : "star"), for: .normal)
            ratingButton_3.setImage(UIImage(systemName: rating >= 3 ? "star.fill" : "star"), for: .normal)
            ratingButton_4.setImage(UIImage(systemName: rating >= 4 ? "star.fill" : "star"), for: .normal)
            ratingButton_5.setImage(UIImage(systemName: rating >= 5 ? "star.fill" : "star"), for: .normal)
        }
    }
    
    func updateBook(_ book: Book) {
        self.book?.title = book.title
        self.book?.author = book.author
        updateControls()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navigationController = segue.destination as? UINavigationController,
            let editBookVC = navigationController.viewControllers.first as? EditBookViewController {
                editBookVC.book = book
                editBookVC.delegate = self
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
